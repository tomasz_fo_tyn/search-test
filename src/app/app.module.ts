import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DataModule} from '@search-app/data';
import {SearchModule} from './search/search.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    DataModule.forRoot(),
    SearchModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
