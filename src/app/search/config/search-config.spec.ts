import {TestBed} from '@angular/core/testing';
import {SearchConfig} from '../config/search-config';
import {of} from 'rxjs';
import {UserService} from '../../core/services/user/user.service';
import {MerchantService} from '../../core/services/merchant/merchant.service';
import {SearchActor} from '../enums/search-actor';

describe('SearchConfig', () => {

  const usersSpy = jasmine.createSpyObj('UserService', ['search$']);
  const merchantsSpy = jasmine.createSpyObj('MerchantService', ['search$']);

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: UserService, useValue: usersSpy },
      { provide: MerchantService, useValue: merchantsSpy },
    ]
  }));

  it('should be created', () => {
    const service: SearchConfig = TestBed.get(SearchConfig);
    expect(service).toBeTruthy();
  });

  describe('when providing sources', () => {

    it('should return users', (done) => {
      usersSpy.search$.and.returnValue(of([{}]));
      merchantsSpy.search$.and.returnValue(of([]));
      const service: SearchConfig = TestBed.get(SearchConfig);
      service.getSources('').subscribe(r => {
        const user = [].concat(...r)[0];
        expect(user).not.toBeNull();
        done();
      });
    });

    it('should return merchants', (done) => {
      usersSpy.search$.and.returnValue(of([]));
      merchantsSpy.search$.and.returnValue(of([{}]));
      const service: SearchConfig = TestBed.get(SearchConfig);
      service.getSources('').subscribe(r => {
        const merchant = [].concat(...r)[0];
        expect(merchant).not.toBeNull();
        done();
      });
    });

    it('should convert actors to search results', (done) => {
      usersSpy.search$.and.returnValue(of([{id: '', name: 'b', personId: ''}]));
      merchantsSpy.search$.and.returnValue(of([{id: '', name: 'a', personId: ''}]));
      const service: SearchConfig = TestBed.get(SearchConfig);
      service.getSources('').subscribe(r => {
        const user = [].concat(...r)[1];
        const merchant = [].concat(...r)[0];
        expect(user.type).toEqual(SearchActor.USER);
        expect(user.name).toEqual('b');
        expect(merchant.name).toEqual('a');
        expect(merchant.type).toEqual(SearchActor.MERCHANT);
        done();
      });
    });
  });

  describe('when getting icon by actor', () => {

    it('should return user icon for user', () => {
      const service: SearchConfig = TestBed.get(SearchConfig);
      expect(service.getIconByActor(SearchActor.USER)).toEqual('fa-user');
    });

    it('should return merchant icon for merchant', () => {
      const service: SearchConfig = TestBed.get(SearchConfig);
      expect(service.getIconByActor(SearchActor.MERCHANT)).toEqual('fa-user-tie');
    });
  });
});
