import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {SearchResult} from '../models/search-result';
import {SearchActor} from '../enums/search-actor';
import {MerchantService} from '../../core/services/merchant/merchant.service';
import {UserService} from '../../core/services/user/user.service';
import {forkJoin, Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class SearchConfig {

  constructor(private merchantService: MerchantService, private userService: UserService) {}

  public getSources(query: string): Observable<any[]> {
    return forkJoin([
      this.merchantService.search$(query).pipe(
        map(ms => ms.map(m => new SearchResult(SearchActor.MERCHANT, m.name, m.vat)))
      ),
      this.userService.search$(query).pipe(
        map(us => us.map(u => new SearchResult(SearchActor.USER, u.name, u.personId)))
      ),
    ]);
  }

  public getIconByActor(actor: SearchActor): string {
    let result;
    switch (actor) {
      case SearchActor.USER:
        result = 'fa-user';
        break;
      case SearchActor.MERCHANT:
        result = 'fa-user-tie';
        break;
      default:
        result = 'fa-image';
        break;
    }
    return result;
  }
}
