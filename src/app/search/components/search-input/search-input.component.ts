import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
})
export class SearchInputComponent implements OnInit {

  @Input() disabled: boolean;
  @Output() changed: EventEmitter<string> = new EventEmitter<string>();

  public searchInputFormControl: FormControl;

  constructor() { }

  ngOnInit() {
    this.initFormControls();
  }

  public search() {
    this.changed.emit(this.searchInputFormControl.value);
  }

  private initFormControls() {
    this.searchInputFormControl = new FormControl('');
  }
}
