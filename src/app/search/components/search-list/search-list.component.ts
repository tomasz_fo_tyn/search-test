import {Component, EventEmitter, Input, Output} from '@angular/core';
import {SearchResult} from '../../models/search-result';

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.scss']
})
export class SearchListComponent {

  @Input() results: SearchResult[];
  @Output() saved: EventEmitter<any> = new EventEmitter<any>();

  public save() {
    this.saved.emit(null);
  }

}
