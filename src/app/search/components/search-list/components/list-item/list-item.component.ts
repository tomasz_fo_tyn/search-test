import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {SearchResult} from '../../../../models/search-result';
import {SearchConfig} from '../../../../config/search-config';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-search-list-item',
  templateUrl: './list-item.component.html',
})
export class ListItemComponent implements OnInit {

  @Input() model: SearchResult;
  public iconClass: string;

  constructor(private searchConfig: SearchConfig) {}


  ngOnInit(): void {
    this.iconClass = this.searchConfig.getIconByActor(this.model.type);
  }
}
