import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {SearchListComponent} from './components/search-list/search-list.component';
import {ListItemComponent} from './components/search-list/components/list-item/list-item.component';
import {CommonModule} from '@angular/common';
import {SearchInputComponent} from './components/search-input/search-input.component';
import {NoSearchResultsComponent} from './components/no-search-results/no-search-results.component';
import {SearchErrorComponent} from './components/search-error/search-error.component';
import {SearchSpinnerComponent} from './components/search-spinner/search-spinner.component';
import {SearchComponent} from './search.component';

@NgModule({
  declarations: [
    SearchComponent,
    SearchInputComponent,
    NoSearchResultsComponent,
    SearchErrorComponent,
    SearchSpinnerComponent,
    SearchListComponent,
    ListItemComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [
    SearchComponent
  ],
  providers: []
})
export class SearchModule {}

