import {Component, OnInit} from '@angular/core';
import {tap} from 'rxjs/operators';
import {SearchResult} from './models/search-result';
import {SearchService} from './services/search.service';
import {ExportService} from '../export/services/export.service';
import {ExportFormat} from '../export/enums/export-format';
import {ExportFileNameService} from './services/export-file-name/export-file-name.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit {

  public isSearching: boolean;
  public doesSearchFailed: boolean;
  public isSearchEmpty: boolean;
  public lastSearchCriteria: string;
  public results: SearchResult[];

  constructor(private searchService: SearchService, private exportService: ExportService,
              private exportFileNameService: ExportFileNameService) {}

  ngOnInit(): void {
    this.results = [];
  }

  public search(value: string) {
    this.lastSearchCriteria = value;
    this.isSearching = true;
    this.doesSearchFailed = false;
    this.isSearchEmpty = false;
    this.searchService.search$(value)
      .pipe(
        tap(result => this.isSearchEmpty = result.length === 0),
      )
      .subscribe(
        result => this.results = result,
        () => {
          this.doesSearchFailed = true;
          this.isSearching = false;
        },
        () => this.isSearching = false
      );
  }

  public save() {
    const fileName = this.exportFileNameService.createFileName(this.lastSearchCriteria);
    this.exportService.export(this.results, fileName, ExportFormat.CSV);
  }
}
