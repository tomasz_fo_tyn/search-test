import {SearchActor} from '../enums/search-actor';

export class SearchResult {
  constructor(
    public type: SearchActor,
    public name: string,
    public identifier: string,
  ) {}
}
