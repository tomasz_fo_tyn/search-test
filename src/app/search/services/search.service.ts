import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {SearchResult} from '../models/search-result';
import {SearchConfig} from '../config/search-config';

@Injectable({ providedIn: 'root' })
export class SearchService {

  constructor(private config: SearchConfig) {
  }

  public search$(query: string): Observable<SearchResult[]> {
    return this.config.getSources(query).pipe(
      map(result => [].concat(...result)),
      map(result => result.sort(this.sortFunction)),
    );
  }

  private sortFunction(a, b) {
    if (a.name < b.name) {
      return -1;
    } else if (a.name > b.name) {
      return 1;
    } else {
      return 0;
    }
  }
}

