import {TestBed} from '@angular/core/testing';
import {SearchService} from './search.service';
import {SearchConfig} from '../config/search-config';
import {of} from 'rxjs';
import {SearchResult} from '../models/search-result';
import {SearchActor} from '../enums/search-actor';

describe('SearchService', () => {
  const spy = jasmine.createSpyObj('SearchConfig', ['getSources']);
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: SearchConfig, useValue: spy }
    ]
  }));

  it('should be created', () => {
    const service: SearchService = TestBed.get(SearchService);
    expect(service).toBeTruthy();
  });

  describe('when searching', () => {
    it('should return stubbed value from a spy', (done) => {
      spy.getSources.and.returnValue(of([]));
      const service: SearchService = TestBed.get(SearchService);
      service.search$('').subscribe(r => {
        expect(r).toEqual([]);
        done();
      });
    });

    it('should return stubbed value sorted by name', (done) => {
      spy.getSources.and.returnValue(of([
        [
          new SearchResult(SearchActor.USER, 'c', ''),
          new SearchResult(SearchActor.USER, 'a', '')
        ],
        [new SearchResult(SearchActor.MERCHANT, 'b', '')]
      ]));
      const service: SearchService = TestBed.get(SearchService);
      service.search$('').subscribe(r => {
        expect(r.map(item => item.name)).toEqual(['a', 'b', 'c']);
        done();
      });
    });

  });
});
