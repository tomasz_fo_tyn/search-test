import {Injectable} from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class ExportFileNameService {

  constructor() { }

  public createFileName(searchCriteria): string {
    const firstPart = (searchCriteria != null) ? searchCriteria
      .toLowerCase()
      .replace(/ /g, '-') : '';
    const middlePart = (firstPart !== '') ? '-' : '';
    const lastPart = moment(new Date()).format('YYYY-MM-DD');
    return `${firstPart}${middlePart}${lastPart}`;
  }
}
