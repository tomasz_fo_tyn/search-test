import {TestBed} from '@angular/core/testing';
import * as moment from 'moment';

import { ExportFileNameService } from './export-file-name.service';

describe('ExportFileNameService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExportFileNameService = TestBed.get(ExportFileNameService);
    expect(service).toBeTruthy();
  });

  describe('When provided with search criteria', () => {
    const searchCriteria = 'a B c';
    const date = moment(new Date());
    const expectedResult = `a-b-c-${date.format('YYYY-MM-DD')}`;

    it('it should create file name with format {search-criteria}-{year-month-day}', () => {
      const service: ExportFileNameService = TestBed.get(ExportFileNameService);
      const result = service.createFileName(searchCriteria);
      expect(result).toEqual(expectedResult);
    });

  });

  describe('When provided with empty search criteria', () => {
    const searchCriteria = '';
    const date = moment(new Date());
    const expectedResult = `${date.format('YYYY-MM-DD')}`;

    it('it should create file name with format {year-month-day}', () => {
      const service: ExportFileNameService = TestBed.get(ExportFileNameService);
      const result = service.createFileName(searchCriteria);
      expect(result).toEqual(expectedResult);
    });

  });

  describe('When provided with no search criteria', () => {
    const searchCriteria = null;
    const date = moment(new Date());
    const expectedResult = `${date.format('YYYY-MM-DD')}`;

    it('it should create file name with format {year-month-day}', () => {
      const service: ExportFileNameService = TestBed.get(ExportFileNameService);
      const result = service.createFileName(searchCriteria);
      expect(result).toEqual(expectedResult);
    });

  });
});
