import {Injectable} from '@angular/core';
import {ExportToCSVService} from './formats/export-to-csv.service';
import {ExportFormat} from '../enums/export-format';
import {SearchResult} from '../../search/models/search-result';

@Injectable({providedIn: 'root'})
export class ExportService {
  constructor(private exportToCSVService: ExportToCSVService) {}

  public export(data: SearchResult[], fileName: string, format: ExportFormat) {
    this.exportToCSVService.export(data, fileName, ['Type', 'Name', 'Identifier']);
  }
}
