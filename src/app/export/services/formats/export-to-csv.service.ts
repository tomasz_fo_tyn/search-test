import {ExportToCsv} from 'export-to-csv';
import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class ExportToCSVService {
  constructor() {
  }

  public export(data: any[], fileName: string, headers: string[]) {
    const csvExporter = new ExportToCsv(this.getOptions(fileName, headers));
    csvExporter.generateCsv(data);
  }

  private getOptions(fileName: string, headers: string[]): any {
    return {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: false,
      filename: fileName,
      useTextFile: false,
      useBom: true,
      headers,
    };
  }
}
