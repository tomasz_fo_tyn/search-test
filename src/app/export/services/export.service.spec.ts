import {TestBed} from '@angular/core/testing';
import {ExportService} from './export.service';
import {ExportToCSVService} from './formats/export-to-csv.service';

describe('ExportService', () => {
  const spy = jasmine.createSpyObj('ExportToCSVService', ['export']);

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: ExportToCSVService, useValue: spy }
    ]
  }));

  it('should be created', () => {
    const service: ExportService = TestBed.get(ExportService);
    expect(service).toBeTruthy();
  });
});
