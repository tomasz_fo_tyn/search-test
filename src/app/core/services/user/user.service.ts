import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserRepository} from '@search-app/data';
import {User} from '../../models/user';

@Injectable({ providedIn: 'root' })
export class UserService {

  constructor(private repo: UserRepository) { }

  search$(query: string): Observable<User[]> {
    return this.repo.search$(query);
  }
}
